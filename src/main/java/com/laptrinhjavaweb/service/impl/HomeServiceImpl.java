package com.laptrinhjavaweb.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.laptrinhjavaweb.service.HomeService;

@Service
public class HomeServiceImpl implements HomeService{

    @Override
    public List<String> loadMenu() {
        List<String> menus = new ArrayList<String>();
        menus.add("Trang chủ");
        menus.add("Spring MVC");
        menus.add("Java Boot");
        menus.add("Java Core");
        menus.add("Liên hệ");

        return menus;
    }

}
