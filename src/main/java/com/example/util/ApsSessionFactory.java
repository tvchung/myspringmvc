package com.example.util;

import java.io.IOException;
import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.example.dao.EmployeesMapper;
import com.example.model.Employees;

public class ApsSessionFactory {
    // SqlSessionFacory
    private static SqlSessionFactory sqlSessionFactory;

    public static void init() {
        try {
            InputStream inputStream = Resources.getResourceAsStream("mybatis-config.xml");
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (Exception e) {
            System.out.println("mybatis-config.xmlファイルが見つかりません。" + e.toString());
        }

    }

    public static SqlSession openSession() {
        init();
        return sqlSessionFactory.openSession();
    }

    public static void main(String[] args) throws IOException {
        try {
            SqlSession sqlSession = openSession();
            try {
                // ActorテーブルのMapperを取得します(4)
                EmployeesMapper map = sqlSession.getMapper(EmployeesMapper.class);
                // Actorテーブルの主キー（actor_id)が１であるレコードを検索します(5)
                Employees employees = map.selectByPrimaryKey(100);

                // 取得した内容を確認します
                System.out.println("Employess Infor: " + employees.toString());

            } finally {
                sqlSession.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
