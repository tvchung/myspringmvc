package com.example.util;

import org.apache.ibatis.session.SqlSession;

public abstract class AbstractBaseDbLogic {
	protected static SqlSession openSession() {
		return ApsSessionFactory.openSession();
	}

}
