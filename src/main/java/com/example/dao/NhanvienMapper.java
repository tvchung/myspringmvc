package com.example.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.example.model.Nhanvien;
import com.example.model.NhanvienExample;

public interface NhanvienMapper {
    long countByExample(NhanvienExample example);

    int deleteByExample(NhanvienExample example);

    int deleteByPrimaryKey(Short ID);

    int insert(Nhanvien record);

    int insertSelective(Nhanvien record);

    List<Nhanvien> selectByExample(NhanvienExample example);

    Nhanvien selectByPrimaryKey(int ID);

    int updateByExampleSelective(@Param("record") Nhanvien record, @Param("example") NhanvienExample example);

    int updateByExample(@Param("record") Nhanvien record, @Param("example") NhanvienExample example);

    int updateByPrimaryKeySelective(Nhanvien record);

    int updateByPrimaryKey(Nhanvien record);
}