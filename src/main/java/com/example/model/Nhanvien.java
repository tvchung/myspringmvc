package com.example.model;

import java.io.Serializable;

public class Nhanvien implements Serializable {
    private Short ID;

    private String NAME;

    private static final long serialVersionUID = 1L;

    public Short getID() {
        return ID;
    }

    public void setID(Short ID) {
        this.ID = ID;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME == null ? null : NAME.trim();
    }
}