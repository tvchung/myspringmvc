package com.example.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class EmployeesExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public EmployeesExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEMPLOYEE_IDIsNull() {
            addCriterion("EMPLOYEE_ID is null");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDIsNotNull() {
            addCriterion("EMPLOYEE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDEqualTo(Integer value) {
            addCriterion("EMPLOYEE_ID =", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDNotEqualTo(Integer value) {
            addCriterion("EMPLOYEE_ID <>", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDGreaterThan(Integer value) {
            addCriterion("EMPLOYEE_ID >", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDGreaterThanOrEqualTo(Integer value) {
            addCriterion("EMPLOYEE_ID >=", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDLessThan(Integer value) {
            addCriterion("EMPLOYEE_ID <", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDLessThanOrEqualTo(Integer value) {
            addCriterion("EMPLOYEE_ID <=", value, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDIn(List<Integer> values) {
            addCriterion("EMPLOYEE_ID in", values, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDNotIn(List<Integer> values) {
            addCriterion("EMPLOYEE_ID not in", values, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDBetween(Integer value1, Integer value2) {
            addCriterion("EMPLOYEE_ID between", value1, value2, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andEMPLOYEE_IDNotBetween(Integer value1, Integer value2) {
            addCriterion("EMPLOYEE_ID not between", value1, value2, "EMPLOYEE_ID");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEIsNull() {
            addCriterion("FIRST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEIsNotNull() {
            addCriterion("FIRST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEEqualTo(String value) {
            addCriterion("FIRST_NAME =", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMENotEqualTo(String value) {
            addCriterion("FIRST_NAME <>", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEGreaterThan(String value) {
            addCriterion("FIRST_NAME >", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEGreaterThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME >=", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMELessThan(String value) {
            addCriterion("FIRST_NAME <", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMELessThanOrEqualTo(String value) {
            addCriterion("FIRST_NAME <=", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMELike(String value) {
            addCriterion("FIRST_NAME like", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMENotLike(String value) {
            addCriterion("FIRST_NAME not like", value, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEIn(List<String> values) {
            addCriterion("FIRST_NAME in", values, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMENotIn(List<String> values) {
            addCriterion("FIRST_NAME not in", values, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMEBetween(String value1, String value2) {
            addCriterion("FIRST_NAME between", value1, value2, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andFIRST_NAMENotBetween(String value1, String value2) {
            addCriterion("FIRST_NAME not between", value1, value2, "FIRST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEIsNull() {
            addCriterion("LAST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEIsNotNull() {
            addCriterion("LAST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEEqualTo(String value) {
            addCriterion("LAST_NAME =", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMENotEqualTo(String value) {
            addCriterion("LAST_NAME <>", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEGreaterThan(String value) {
            addCriterion("LAST_NAME >", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEGreaterThanOrEqualTo(String value) {
            addCriterion("LAST_NAME >=", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMELessThan(String value) {
            addCriterion("LAST_NAME <", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMELessThanOrEqualTo(String value) {
            addCriterion("LAST_NAME <=", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMELike(String value) {
            addCriterion("LAST_NAME like", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMENotLike(String value) {
            addCriterion("LAST_NAME not like", value, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEIn(List<String> values) {
            addCriterion("LAST_NAME in", values, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMENotIn(List<String> values) {
            addCriterion("LAST_NAME not in", values, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMEBetween(String value1, String value2) {
            addCriterion("LAST_NAME between", value1, value2, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andLAST_NAMENotBetween(String value1, String value2) {
            addCriterion("LAST_NAME not between", value1, value2, "LAST_NAME");
            return (Criteria) this;
        }

        public Criteria andEMAILIsNull() {
            addCriterion("EMAIL is null");
            return (Criteria) this;
        }

        public Criteria andEMAILIsNotNull() {
            addCriterion("EMAIL is not null");
            return (Criteria) this;
        }

        public Criteria andEMAILEqualTo(String value) {
            addCriterion("EMAIL =", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotEqualTo(String value) {
            addCriterion("EMAIL <>", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILGreaterThan(String value) {
            addCriterion("EMAIL >", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILGreaterThanOrEqualTo(String value) {
            addCriterion("EMAIL >=", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLessThan(String value) {
            addCriterion("EMAIL <", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLessThanOrEqualTo(String value) {
            addCriterion("EMAIL <=", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILLike(String value) {
            addCriterion("EMAIL like", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotLike(String value) {
            addCriterion("EMAIL not like", value, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILIn(List<String> values) {
            addCriterion("EMAIL in", values, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotIn(List<String> values) {
            addCriterion("EMAIL not in", values, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILBetween(String value1, String value2) {
            addCriterion("EMAIL between", value1, value2, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andEMAILNotBetween(String value1, String value2) {
            addCriterion("EMAIL not between", value1, value2, "EMAIL");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERIsNull() {
            addCriterion("PHONE_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERIsNotNull() {
            addCriterion("PHONE_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBEREqualTo(String value) {
            addCriterion("PHONE_NUMBER =", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERNotEqualTo(String value) {
            addCriterion("PHONE_NUMBER <>", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERGreaterThan(String value) {
            addCriterion("PHONE_NUMBER >", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERGreaterThanOrEqualTo(String value) {
            addCriterion("PHONE_NUMBER >=", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERLessThan(String value) {
            addCriterion("PHONE_NUMBER <", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERLessThanOrEqualTo(String value) {
            addCriterion("PHONE_NUMBER <=", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERLike(String value) {
            addCriterion("PHONE_NUMBER like", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERNotLike(String value) {
            addCriterion("PHONE_NUMBER not like", value, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERIn(List<String> values) {
            addCriterion("PHONE_NUMBER in", values, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERNotIn(List<String> values) {
            addCriterion("PHONE_NUMBER not in", values, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERBetween(String value1, String value2) {
            addCriterion("PHONE_NUMBER between", value1, value2, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andPHONE_NUMBERNotBetween(String value1, String value2) {
            addCriterion("PHONE_NUMBER not between", value1, value2, "PHONE_NUMBER");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEIsNull() {
            addCriterion("HIRE_DATE is null");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEIsNotNull() {
            addCriterion("HIRE_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEEqualTo(Date value) {
            addCriterion("HIRE_DATE =", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATENotEqualTo(Date value) {
            addCriterion("HIRE_DATE <>", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEGreaterThan(Date value) {
            addCriterion("HIRE_DATE >", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEGreaterThanOrEqualTo(Date value) {
            addCriterion("HIRE_DATE >=", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATELessThan(Date value) {
            addCriterion("HIRE_DATE <", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATELessThanOrEqualTo(Date value) {
            addCriterion("HIRE_DATE <=", value, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEIn(List<Date> values) {
            addCriterion("HIRE_DATE in", values, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATENotIn(List<Date> values) {
            addCriterion("HIRE_DATE not in", values, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATEBetween(Date value1, Date value2) {
            addCriterion("HIRE_DATE between", value1, value2, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andHIRE_DATENotBetween(Date value1, Date value2) {
            addCriterion("HIRE_DATE not between", value1, value2, "HIRE_DATE");
            return (Criteria) this;
        }

        public Criteria andJOB_IDIsNull() {
            addCriterion("JOB_ID is null");
            return (Criteria) this;
        }

        public Criteria andJOB_IDIsNotNull() {
            addCriterion("JOB_ID is not null");
            return (Criteria) this;
        }

        public Criteria andJOB_IDEqualTo(String value) {
            addCriterion("JOB_ID =", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDNotEqualTo(String value) {
            addCriterion("JOB_ID <>", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDGreaterThan(String value) {
            addCriterion("JOB_ID >", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDGreaterThanOrEqualTo(String value) {
            addCriterion("JOB_ID >=", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDLessThan(String value) {
            addCriterion("JOB_ID <", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDLessThanOrEqualTo(String value) {
            addCriterion("JOB_ID <=", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDLike(String value) {
            addCriterion("JOB_ID like", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDNotLike(String value) {
            addCriterion("JOB_ID not like", value, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDIn(List<String> values) {
            addCriterion("JOB_ID in", values, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDNotIn(List<String> values) {
            addCriterion("JOB_ID not in", values, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDBetween(String value1, String value2) {
            addCriterion("JOB_ID between", value1, value2, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andJOB_IDNotBetween(String value1, String value2) {
            addCriterion("JOB_ID not between", value1, value2, "JOB_ID");
            return (Criteria) this;
        }

        public Criteria andSALARYIsNull() {
            addCriterion("SALARY is null");
            return (Criteria) this;
        }

        public Criteria andSALARYIsNotNull() {
            addCriterion("SALARY is not null");
            return (Criteria) this;
        }

        public Criteria andSALARYEqualTo(BigDecimal value) {
            addCriterion("SALARY =", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYNotEqualTo(BigDecimal value) {
            addCriterion("SALARY <>", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYGreaterThan(BigDecimal value) {
            addCriterion("SALARY >", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SALARY >=", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYLessThan(BigDecimal value) {
            addCriterion("SALARY <", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SALARY <=", value, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYIn(List<BigDecimal> values) {
            addCriterion("SALARY in", values, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYNotIn(List<BigDecimal> values) {
            addCriterion("SALARY not in", values, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SALARY between", value1, value2, "SALARY");
            return (Criteria) this;
        }

        public Criteria andSALARYNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SALARY not between", value1, value2, "SALARY");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTIsNull() {
            addCriterion("COMMISSION_PCT is null");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTIsNotNull() {
            addCriterion("COMMISSION_PCT is not null");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTEqualTo(BigDecimal value) {
            addCriterion("COMMISSION_PCT =", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTNotEqualTo(BigDecimal value) {
            addCriterion("COMMISSION_PCT <>", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTGreaterThan(BigDecimal value) {
            addCriterion("COMMISSION_PCT >", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("COMMISSION_PCT >=", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTLessThan(BigDecimal value) {
            addCriterion("COMMISSION_PCT <", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTLessThanOrEqualTo(BigDecimal value) {
            addCriterion("COMMISSION_PCT <=", value, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTIn(List<BigDecimal> values) {
            addCriterion("COMMISSION_PCT in", values, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTNotIn(List<BigDecimal> values) {
            addCriterion("COMMISSION_PCT not in", values, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMMISSION_PCT between", value1, value2, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andCOMMISSION_PCTNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("COMMISSION_PCT not between", value1, value2, "COMMISSION_PCT");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDIsNull() {
            addCriterion("MANAGER_ID is null");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDIsNotNull() {
            addCriterion("MANAGER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDEqualTo(Integer value) {
            addCriterion("MANAGER_ID =", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDNotEqualTo(Integer value) {
            addCriterion("MANAGER_ID <>", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDGreaterThan(Integer value) {
            addCriterion("MANAGER_ID >", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDGreaterThanOrEqualTo(Integer value) {
            addCriterion("MANAGER_ID >=", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDLessThan(Integer value) {
            addCriterion("MANAGER_ID <", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDLessThanOrEqualTo(Integer value) {
            addCriterion("MANAGER_ID <=", value, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDIn(List<Integer> values) {
            addCriterion("MANAGER_ID in", values, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDNotIn(List<Integer> values) {
            addCriterion("MANAGER_ID not in", values, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDBetween(Integer value1, Integer value2) {
            addCriterion("MANAGER_ID between", value1, value2, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andMANAGER_IDNotBetween(Integer value1, Integer value2) {
            addCriterion("MANAGER_ID not between", value1, value2, "MANAGER_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDIsNull() {
            addCriterion("DEPARTMENT_ID is null");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDIsNotNull() {
            addCriterion("DEPARTMENT_ID is not null");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDEqualTo(Short value) {
            addCriterion("DEPARTMENT_ID =", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDNotEqualTo(Short value) {
            addCriterion("DEPARTMENT_ID <>", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDGreaterThan(Short value) {
            addCriterion("DEPARTMENT_ID >", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDGreaterThanOrEqualTo(Short value) {
            addCriterion("DEPARTMENT_ID >=", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDLessThan(Short value) {
            addCriterion("DEPARTMENT_ID <", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDLessThanOrEqualTo(Short value) {
            addCriterion("DEPARTMENT_ID <=", value, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDIn(List<Short> values) {
            addCriterion("DEPARTMENT_ID in", values, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDNotIn(List<Short> values) {
            addCriterion("DEPARTMENT_ID not in", values, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDBetween(Short value1, Short value2) {
            addCriterion("DEPARTMENT_ID between", value1, value2, "DEPARTMENT_ID");
            return (Criteria) this;
        }

        public Criteria andDEPARTMENT_IDNotBetween(Short value1, Short value2) {
            addCriterion("DEPARTMENT_ID not between", value1, value2, "DEPARTMENT_ID");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}