package com.java.test;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import com.example.dao.NhanvienMapper;
import com.example.model.Nhanvien;
public class MyBatisTest {

    public static void main(String[] args) {
        try {
            String resource = "mybatis-config.xml";
            InputStream inputStream = Resources.getResourceAsStream(resource);
            SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
            SqlSession session = sqlSessionFactory.openSession();
            try {
                NhanvienMapper mapper = session.getMapper(NhanvienMapper.class);
                Nhanvien personInfos = mapper.selectByPrimaryKey(2);
                if (personInfos == null) {
                    System.out.println("The result is null.");
                } else {
                    System.out.println("name:" + personInfos.getNAME());
//                    for (Nhanvien personInfo : personInfos) {
//                        System.out.println("---PersonInfo---");
//                        System.out.println("name:" + personInfo.getNAME());
//                    }
                }
            } finally {
                session.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
